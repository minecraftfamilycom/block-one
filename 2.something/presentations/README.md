# About the Minecraft-Family.com Curriculum
## Practical Programming and Design Instructions
Minecraft game mechanics are incredibly analogous to engineering. Our programs focus on teaching problem solving and programming techniques through Minecraft, Lua, Java and the wild web.

# The Blocks
Our curriculum is organized into a core curriculum called Blocks, which allow for a logical progression through increasingly complex concepts.

## Block 1: The Basics & Getting Started
Learn the basics of computers
For ages 6-12.

## Block 2: Basic Programming Concepts
Turtles
Computers