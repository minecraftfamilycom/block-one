# The Command Line
This lesson will give some more insights into what a Command Line or CLI is.
We will refer to some real CLI's in Windows & Mac.
And show some benefits and examples you can use in the CLI.
After that we continue with explaining Minecraft Commands and how to use them.
And we finish with some.

## Where can we find the command line?
In Minecraft the command line can be found via the chat.
Type T and to start a command you have to prefix it with slash[ / ].

```
/give me command_block
```

To get an overview of what is possible in the Minecraft command line we can type

```
/help
```
This will bring up the help menu which explains all the possible commands that are available.
![Screen Shot 2017-02-12 at 23.29.17.png](images/Screen17.png)
![Screen Shot 2017-02-12 at 23.29.50.png](images/Screen50.png)

## Where else do we find the Command Line?
Mac - cmd + space -> terminal ->
```
ls -l
```
![Screen Shot 2017-02-15 at 13.49.07.png](images/Screen07.png)

Windows - start -> cmd ->
```
dir
```
![Screen Shot 2017-02-15 at 13.47.05.png](images/Screen05.png)

We opened a command line and gave it a command to list up all the directories of where we are located. As you can see when you list up the directories, in both cases it reflects the structure of your Explorer/Finder screen.

## What is the Command Line?
A command line interface or CLI is a method of interacting with a computer by giving it lines of text commands in written form.
The computer then generally responds with text output to the display or to a file .
*Speaking to the Heart of the PC*
The reason that we are learning this is that when we get started with ComputerCraft we need to manage our code and files.

## How to use the Command line?
- Exploring directories, manage files, open files - everything you do now with your mouse.
- Launching software,
- Scripting language
- - you can do a little programming in the CLI. Or write code into files and run them as scripts in the CLI.
- Batch Processing
- - means that you would would repeat a task several times
- - rename 1000 pictures,
- - rename you series for your video manager
- Kill a program
- - when you are stuck and your app is frozen you can always kill every program fast with the CLI
- Chat is obvious or you can write your own chat application and have a secret communication system for you and your friends
- Access other pc’s
- - access your desktop/nas from your laptop or vice versa, this means that you can store all your videos on the desktop and access them via your laptop
- - connect a mac to play games on your windows desktop
- Rsync, which stands for "remote sync", is a remote and local file synchronization tool. It uses an algorithm that minimizes the amount of data copied by only moving the portions of files that have changed.
- Wget GNU Wget is a free software package for downloading files using HTTP, HTTPS and FTP, the most widely-used Internet protocols.
- - when you have a desktop and laptop you can keep all your files in sync with a single command on both devices
- Mplayer is a movie player for Linux, a minimalist command line interface to MPD.

# Minecraft Commands
## How can we use them?
There are cheats and game commands that you can use to change:

- game modes,
- time,
- weather,
- summon mobs or objects,
- find the seed used by the World Generator
- ...

## Target Selectors
A target selector variable identifies the broad category of targets to select. Several command have a different set of Arguments. There are four variables:
Example:
```
@a, @e, @p, @r
```


![Screen Shot 2017-02-15 at 21.05.14.png](images/Screen14.png)

Example:
```
/command @<variable>[<argument>=<value>, <argument>=<value>]
/gamemode creative @a[team=Red]
/kill @e[type=Creeper, r=20]
/give @a[r=50] Stone

/command 		@<variable>	[<argument>=<value>, <argument>=<value>]
/gamemode creative 	@a		[team=Red, 	     m=Adventure]
/kill 		        @e		[type=Creeper, 	     r=20]
/give 		        @a		[r=50] Stone
```

## Data Tags
A data tag is a sequence of text which describes a data structure using attribute-value pairs. Data tags are used in commands to specify complex data for players, entities, and some blocks.

![Screen Shot 2017-02-15 at 21.08.24.png](images/Screen24.png)

Example:
```
{name1:123,name2:"sometext",name3:{subname1:456,subname2:789}}
```
## Command Types
There are several different command groups

- Server
- Weather
- Game Mode
- Time
- ...

A lot of the examples can be found on [DigMinecraft](https://www.digminecraft.com/game_commands/index.php) but of course all the commands are well explained on the [Minecraft Wiki]

(http://minecraft.gamepedia.com/Commands)
![Screen Shot 2017-02-14 at 12.02.34.png](images/Screen34.png)
![Screen Shot 2017-02-14 at 12.02.21.png](images/Screen21.png)
![Screen Shot 2017-02-14 at 12.02.12.png](images/Screen12.png)

# Let's Code
Now we can start trying out the stuff we learned

- Presentation
- - You can find a presentation about this lesson in this repo as well.

## Minecraft
- Play Commands Level created by Zorth
- - You can find the world in this repo as well
- - It's a short example of what you can build with the knowledge of commands and command blocks. But it's also a short introduction into using commands.

- Run through Commands [DigMinecraft](https://www.digminecraft.com/game_commands/index.php)
- - DigMinecraft has a nice set of examples to teach you how to write awesome commands.
- - Save every command you try out in a command block and save the world, it will come in handy later.
- - Give yourself a command block with: ``` /give @a command_block ```

- - Some /commands you should try out:
- - - ``` /clone```
- - - ``` /scoreboard```
- - - ``` /setblock```
- - - ``` /testfor```
- - - ``` /execute```
- - - ``` /fill```
- - - ``` /gamerule```
- - - ``` /give```
- - - ``` /summon```
## Command Line
This section will help you further with the command line on Windows or Mac.

- [Terminus](http://web.mit.edu/mprat/Public/web/Terminus/Web/main.html)
- - Is a command line game with ASCII art to teach you the ins and outs
- [Learning the Shell](http://linuxcommand.org/learning_the_shell.php)
- - Where Terminus is a game to teach, this is a deeper and more elaborated explenation of the commands